﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace yapımarket
{
    public class Ürünler
    {
        public int ID
        {
            get;
            set;
        }
        public string Adi
        {
            get;
            set;
        }
        public int FirmaID
        {
            get;
            set;
        }
        public int KategoriID
        {
            get;
            set;
        }
    }
}